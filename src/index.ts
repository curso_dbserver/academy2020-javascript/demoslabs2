class Moeda {
    constructor(public valor: number, public nome: string) {
    }
}
class Cofrinho {
    #moedas: Moeda[] = [];
    adicionar(m: Moeda) {
        this.#moedas.push(m);
    }
    calcularTotal(): number {
        const somador: (s: number, m: Moeda) => number =
                       (soma, moeda) => soma + moeda.valor;
        return this.#moedas.reduce(somador, 0); 
    }
}

let cofre = new Cofrinho();
cofre.adicionar(new Moeda(1, 'um pila'));
cofre.adicionar(new Moeda(0.5, '50 centados de pila'));
console.log(cofre.calcularTotal());

